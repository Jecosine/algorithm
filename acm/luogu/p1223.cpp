#include <bits/stdc++.h>
using namespace std;
typedef struct {
	int i;
	int t;
} P;
bool cmp(P p1, P p2){
	if(p1.t == p2.t)
		return p1.i < p2.i;
	return p1.t<p2.t;
}
int main(){
	int n;
	cin >> n;
	P a[n];
	for(int i = 0; i < n; i++){
		a[i].i = i+1;
		cin >> a[i].t;
	}
	sort(a, a+n, cmp);
	long long s = 0, t = 0;
	for(int i = 0; i < n; i++){
		cout << a[i].i << " ";
		t += s;
		s += a[i].t;
		
	}
	printf("\n%.2f", (double)t/(double)n);
	return 0;
} 

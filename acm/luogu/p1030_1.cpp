#include <bits/stdc++.h>
using namespace std;
string s1, s2;
void dfs(int l, int r, int x){
  char root = s2[x];
  if(l > r)
    return ;
  cout << root;
  
  
  for(int i = l;i <= r; i++){
    if (s1[i] == root){
      dfs(l, i-1, x - (r - i) - 1);
      dfs(i+1, r, x-1);
    }
  }
}
int main(){
  cin >> s1 >> s2;
  dfs(0, s1.size()-1, s1.size()-1);
  return 0;
}
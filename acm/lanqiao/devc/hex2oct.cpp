#include <iostream>
#include <cstdio>

using namespace std;
void trans(string s);
int main() {
	string s;
	int n;
	scanf("%d\n", &n);
	for(int i = 0;i < n; i++){
		getline(cin, s);	
		trans(s);
	}	
	return 0;
}
void trans(string s){
	string t, result = "";
	// fill zero
	int l = s.size(),fc = (l % 3)?3 - l % 3:0;
	for(int i =0;i < fc;i++)
		s.insert(0,"0");
	int f,r,sum = 0;
	l = f = r = s.size();
//	cout << f << endl;
	f -= 3;
	while(f >= 0){
		
		sum = 0;
		// to 10
		for(int i = 0; i < 3; i++){
			
			if (s[i+f] >= 'A'){
				sum += 10 + s[i+f] - 'A';
			} else {
				sum += s[i+f] - '0';
			}
			if (i < 2)
				sum *= 16;
		}
	
//		cout << sum<<endl;
		// to 8
		while(sum){
			t.insert(0, 1, '0' + sum % 8);
			sum /= 8;
		}
		// fill zero to 4
		fc = 4 - t.size();
		for(int i = 0;i < fc;i++)
			t.insert(0,"0");
		
		result.insert(0, t);		
//		cout << t << endl;
		f -= 3;
		t = "";
	}
	// remove 0
	fc=-1;
	for(int i = 0; result[i]=='0';i++)
		fc = i;
//	cout << "fc:" << fc<< endl;
//	if(fc!=-1)
    result.assign(result, fc+1, result.size()-fc);
	cout << result<<endl;
}

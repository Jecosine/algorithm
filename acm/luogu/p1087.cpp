#include <bits/stdc++.h>
using namespace std;
char judge(string s){
  char f = s[0], ans = (f == '0')?'B':'I';
  int l = (int)s.size();
  for(int i = 1; i < l;i++){
    if(s[i] != f){
      ans = 'F';
      return ans;
    }
  }
  return ans;
}
void build_tree(string s){
  char root = judge(s);
  string l, r;
  int ss = s.size();
  if (ss == 1){
    cout << root;
    return ;
  }
  l.assign(s, 0, ss/2);
  r.assign(s, ss/2, ss/2);
  build_tree(l);
  build_tree(r);
  cout << root;  
}

int main(){
  string s;
  int m;
  cin >> m >> s;
  build_tree(s);
  return 0;
}
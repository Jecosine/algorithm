#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;
typedef struct point{
	int x, y;
	int c;
	point(int i, int j, int k){
		x = i; y = j; c = k;
	}
};
bool cmp(point p1, point p2){
	return p1.c > p2.c;
}
int cost = 0, sum = 0;
point cur = point(-1, 0, 0);
vector<point> nuts;
int f[21][21];
int m, n, k, t;
bool pick(point &p){
	int temp = 0;
	// from start point
	if (cur.x == -1)
		temp = 2 * p.x + 3;
	else 
		temp = abs(p.x - cur.x) + abs(p.y - cur.y)+ p.x + 2 ;
	
	if(cost + temp <= k){
		//sum += p.c;
		cost += temp - 1 - p.x;
		//cout << "Current node: " << cur.x << " form node: " << cur.y - p.y<< "  temp: "<< temp << "  current time:" << cost << endl;
		cur = p;
		
		return true;
	} 
	return false;
	
	
	
}


int main(){
	memset(f, 0, sizeof(f));
	cin >> m >> n >> k;
	for(int i =0 ;i < m; i++)
		for(int j = 0;j< n;j++){
			cin >> t ; if(t) nuts.push_back(point(i, j, t));
		}
	sort(nuts.begin(), nuts.end(), cmp);
	int s =nuts.size();
	// debug
	//for(int i = 0; i < s; i++) cout << nuts[i].c << " "; cout << endl;
	for(int i = 0; i < s; i++){
		//if(nuts[i].c==0) break;
		if(pick(nuts[i]))
			sum += nuts[i].c;
		else
			break;
	}
	cout << sum;
	return 0;
}

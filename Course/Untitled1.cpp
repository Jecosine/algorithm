#include <vector>
#include <cstdio>
#include <iostream>
typedef struct LinkNode {
    int index;
    LinkNode* next;
} *Node;
typedef struct Graph {
    int index;
    int _in;
    Node next;
};
using namespace std;
class Solution {
public:
    vector<Graph> g;
    vector<int> visited;
    vector<int> total;
    bool search_list(vector<int>& list, int e, int& pos){
        for(int i = 0; i < list.size(); i++){
            if (list[i] == e) {
                pos = i;
                return true;
            }
        }
        return false;
    }
    void insert_after(Node& p, Node& s){
        p->next = s;
        s->next = NULL;
    }
    void insert(Node& head, Node& e){
        Node p = head->next;
        Node pre = head;
        while(p){
            if(e->index == p->index)
                break;
            else {
                pre = p;
                p = p->next;
            }
        }
        if(p->index!=e->index)
            insert_after(pre, e);
    }

    void build_graph(vector<Graph>& g, vector<vector<int> > &pre){
        for(int i = 0;i < pre.size(); i++){
            Graph temp;
            temp.index = i;
            temp._in = 0;
            temp.next = new LinkNode;
            g.push_back(temp);
        }
        for(int i = 0;i < pre.size(); i++){
            int head = pre[i][0];
            Graph temp = g[head];
            for(int j = 1; j < pre[i].size();j++){
                int head = pre[i][j];
                Node n = new LinkNode;
                n->index = head;
                n->next = NULL;
                insert(temp.next, n);
                g[head]._in++;
            }
        }

    }
    bool canFinish(int numCourses, vector<vector<int> >& prerequisites) {
        build_graph(g, prerequisites);
        int index = 0;
        vector<int> visited;
        for(int i = 0; i < g.size();i++){
            if (g[i]._in == 0){
                DFS(i, visited);
                break   n
            }
        }
        if(visited.size() != numCourses)
            return false;
        else
            return true;
    }
    void DFS(int index, vector<int>& visited){
        Graph graph = g[index];
        Node p = graph.next;
        visited.push_back(index);
        while(p -> next){
            p = p->next;
            g[p->index]._in --;
            if(graph._in == 0)
                DFS(p->index, visited);
        }
    }
};

int main(){
    Solution s;
    vector<vector<int> > p;
    vector<int> a,b;
    a.push_back(0);
    a.push_back(1);
    b.push_back(1);
    b.push_back(0);
    p.push_back(a);
    p.push_back(b);
    cout << s.canFinish(2, p) << endl;
    return 0;
}

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

typedef struct item {
	int p ;
	int q ;
	int delta;
	int profit;
	item(int px, int qx, int profitx){
		p = px; q = qx; profit = profitx;
	}
};
vector<item> items;
bool cmp(item i1, item i2){
	return i1.p < i2.p;
}
bool flag = false;
int pc, ps;
void test(vector<item>& p, int index, int x){
	int n = p.size();
	p[index].profit = (p[index].p - pc + x) * p[index].q;
	for(int i = 0; i < n; i++){
		p[i].profit = (p[i].p - pc + x) * p[i].q;
		if(p[index].profit < p[i].profit) {
			flag = false;
			return ;
		}
	}
	flag = true;
}
int main(){
	int deltaMax = 100000, deltaMin = -100000;
	int n, x, p, q;
	int profit;
	// government expection
	cin >> n;
	
	// price and sales volumn
	// product cost
	cin >> pc >> ps;
	items.push_back(item(pc, ps, 0));
	while(cin >> p >> q && (p!=-1 && q!=-1)){
		profit = (p - pc) * q;
		items.push_back(item(p, q, profit));		
	}
	// sort
	//sort(items.begin(), items.end(), cmp);
	
	// calc sales volumn in every price
	int tempVolumn, tempDelta, index;
	int l = items.size();
	for(int i = 1; items.begin() + i != items.end(); i+=items[i].p - items[i-1].p){
		
		if(items[i].p - items[i-1].p > 1){
			//cout << "???"<<endl;
			tempVolumn = items[i].q;
			tempDelta = (items[i].q - items[i-1].q) / (items[i].p - items[i-1].p);
			tempVolumn -= tempDelta;
			for(int j = items[i].p - 1; j > items[i-1].p; j--, tempVolumn -= tempDelta){
				profit = (j - pc) * tempVolumn;
				items.insert(items.begin()+i, item(j, tempVolumn, profit));
			}
		}
		//cout << "current i: " << i <<endl;
	}
	int delta;
	cin >> delta;
	profit = 0; q = (items.end()-1)->q; p = (items.end()-1)->p;
	while(profit >= 0){
		++p;
		q -= delta;
		profit = (p-pc) * q;
		//cout << p << " " << q << " " << profit<<endl;
		if (profit >= 0)
			items.push_back(item(p, q, profit));
	}
	l = items.size();
	// expected price is valided?
	if(n <= (items.end()-1)->p && n >= items.begin()->p){
		index = n - items.begin()->p;
		//cout <<  "len: " << l << endl;
		//cout << endl << "index: " << index << " " << items[index].p << endl;
	}
	else {
		cout << "NO SOLUTION";
		return 0;
	}
	
	//for(int i = 0; i < l; i++)
	//		cout << items[i].p << " " << items[i].q << " " << items[i].profit << endl;
		
	// calc min and max
	x = 1;
	while(1){
		for(int i = 0; i < l; i++){
			test(items, index, x);
			if(flag) break;
			test(items, index, -x);
			if(flag) {
				x = -x;
				break;
			}
		} 
		
		// debug 
		//if (x > 31){
		//l = items.size();
		//cout << "x: " << x <<endl;
		//for(int i = 0; i < l; i++)
		//	cout << items[i].p << " " << items[i].profit << endl;
		//}
		if(flag) break;
		x++;
		// debug
		//if (x > 37) break;
	}
	cout << x;
	 
	return 0;
	
}

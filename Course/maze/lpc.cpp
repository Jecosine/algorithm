#include <stdio.h>
#define  TRUE 1
#define  FALSE 0
#define  OK  1
#define  ERROR 0
#define  RANGE   20		//RANGE为实际分配的空间行列数
#define M 8				//maze数组的行数 
#define N 11			// maze数组的列数 
typedef  int  Status;
typedef struct { //m，n为待处理的迷宫的行列数，RANGE为实际分配的空间行列数
	int m,n;
	char arr[RANGE][RANGE];
} MazeType;
typedef struct {		//表达迷宫元素位置信息的坐标
	int r,c,pre; 
} PosType;
PosType Path[500];
typedef struct QNode {
	PosType data;
	struct QNode *next;
} QNode,*QuPtr;
typedef struct {
	QuPtr front;
	QuPtr rear;
} QuType;
Status InitQueue(QuType &Q ) { //构造一个带有头结点的空队列 
	Q.front=Q.rear=new QNode;
	Q.front->next=NULL;
	return OK;
}
Status   QuEmpty(QuType Q) { 	//判断是否为空队列 
	if(Q.rear==Q.front)
		return   TRUE;
	return  FALSE;
}
Status EnQueue(QuType &Q ,PosType e) { //插入元素e为新的队尾元素
    QuPtr P;
	P->data=e;
	P->next=NULL;
	Q.rear->next=P;
	Q.rear=P;
	return OK;
}
Status DeQueue(QuType &Q) { //出队
	if (Q.front==Q.rear)
		return ERROR;
	QNode *p;
	p=Q.front->next;
	Q.front->next=p->next;
	if(Q.rear==p)
		Q.rear==Q.front;
	delete p;
	return OK;
}

PosType   NextPos(PosType curpos,int di) {
	//通过di的值，确定下一步的位置，下一步位置实际是当前位置的四个邻居中的一个
	switch(di) {
		case 1:
			curpos.c++; //向东走
			break;
		case 2:
			curpos.r++; //向南走
			break;
		case 3:
			curpos.c--; //向西走
			break;
		case 4:
			curpos.r--;  //向北走
			break;
	}
	return   curpos;
}
Status   pass(MazeType  maze,PosType  curpos) {
	//判断迷宫Maze中，当前位置curpos是否是一个可达位置
	int   m,n;   //注意这里的m，n只是表达下标的临时变量，与前面出现的m，n是不一样的
	m=curpos.r;
	n=curpos.c;
	if(maze.arr[m][n]==0)
		return   TRUE;
	return   FALSE;
}
Status  SignPath(MazeType  &maze,PosType  curpos){ //标记路径 
	int   m,n;   //注意这里的m，n只是表达下标的临时变量，与前面出现的m，n是不一样的
	m=curpos.r;
	n=curpos.c;
	maze.arr[m][n]=2;
	return   TRUE;	
}
Status  CleanPath(MazeType  &maze,PosType  curpos){ //清除 
	int   m,n;   //注意这里的m，n只是表达下标的临时变量，与前面出现的m，n是不一样的
	m=curpos.r;
	n=curpos.c;
	maze.arr[m][n]=0;
	return   TRUE;	
}
void MazePath_BFS(MazeType &maze,PosType start,PosType end){
	PosType curpos,nextpos;
	int i=0,k=-1;
	QuType L;
	InitQueue(L);
	curpos.r=start.r;
	curpos.c=start.c;
	curpos.pre=-1;
	EnQueue(L,curpos);
	SignPath(maze,curpos); 
    Path[i].r=start.r;
	Path[i].c=start.c;
	Path[i].pre=k;
	while(!QuEmpty(L)){
		DeQueue(L);
		k++;
		for(int j=1;j<5;j++){
			i++;
		    nextpos=NextPos(curpos,j);
		    if(pass(maze,nextpos)){
		    	nextpos.pre=-1;
		        EnQueue(L,nextpos);
		        curpos.pre=k;
				Path[i].r=nextpos.r;
				Path[i].c=nextpos.c;
				Path[i].pre=curpos.pre;	
				SignPath(maze,nextpos);	    	
			}	
		} 
	}	 
} 
void Print(MazeType maze) {
	int i,j;
	printf("表示迷宫的数组\n");
	for(i=0; i<M; i++) {
		printf("\t");
		for(j=0; j<N; j++) {
			printf("%d ",maze.arr[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
void   InitMaze(MazeType  &maze,int  a[][N],int row,int col) {
	//根据二维数组来初始化迷宫，这个二维数组可以设计为由用户从键盘输入，解决不同迷宫的问题，但这里用户接口不是我们考虑的重点
	//数据结构学习时往往将输入的数据直接嵌入到程序中，以简化输入节约时间
	//二维数组名a做参数时，需要知道待读的二维数组的列数，因为C语言中二维数组是按行优先顺序存储的
	//控制每行长度的实际就是定义列的数值，所以要明确参数N
	int i,j;
	maze.m=row;
	maze.n=col;
	for(i=0; i<row; i++)
		for(j=0; j<col; j++) {
			if(a[i][j]==0)
				maze.arr[i][j]=0;
			else
				maze.arr[i][j]=1;
		}
}
int main(){
	int maze[M][N]= {
		1,1,1,1,1,1,1,1,1,1,1,
		1,0,1,0,0,1,1,1,0,0,1,
		1,0,0,0,0,0,1,0,0,1,1,
		1,0,1,1,1,0,0,0,1,1,1,
		1,0,0,0,1,0,1,1,0,1,1,
		1,1,0,0,1,0,1,1,0,0,1,
		1,1,1,0,0,0,0,0,0,0,1,
		1,1,1,1,1,1,1,1,1,1,1
	};
	MazeType L;
	PosType   start,end;
	start.r=1;
	start.c=1;
	end.r=6;
	end.c=9;
	InitMaze(L,maze,M,N);
	Print(L);
	MazePath_BFS(L,start,end);
	Print(L);
	return 0;
	
}
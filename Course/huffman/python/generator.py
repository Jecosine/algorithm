import random
import numpy as np
from PIL import Image
import uuid
charset = ".;',abcdefghijklmnopqrstuvwxyz  1234567890 "
ch_charset = [bytes.fromhex(f'{head:x}{body:x}').decode('gb2312') for head in range(0xb0, 0xf7) for body in range(0xa1, 0xfa)]
def gen_image(type_count = 10, total = 5):
    # 像素信息种类个数    
    for i in range(1, type_count+1):
        r = np.random.randint(0, i,1024*768).reshape((1024,768))
        r = np.dstack((r,r,r))
        r = np.array(r, dtype=np.uint8)
        im = Image.fromarray(r) 
        im.save("data/bin_data/{}.jpg".format(i))


def gen_en_text(size = 1024, filename = "t.txt" ):
    # english
    global charset
    content = ""
    for i  in range(size):
        content += random.choice(charset)
    with open(filename,'w') as f:
        f.write(content)
    #return content

def gen_ch_text(size = 1024):
    head = body = 0
    content = ""
    for i in range(size / 2):
        head = random.randint(0xb0, 0xf7)
        body = random.randint(0xa1, 0xfe)
        val = f'{head:x}{body:x}'
        c = bytes.fromhex(val).decode('gb2312')
        content += c
    with open("t.txt",'w') as f:
        f.write(content)
    return content

def gen_dataset(type_count = 30, file_size = 1024, total_count=5):
    global ch_charset
    cs = ch_charset[:type_count]
    content = ""
    for i in range(total_count):
        content = ""
        for j in range(file_size // 2):
            content += random.choice(cs)
        with open("data/txt_data/{}-{}-{}.txt".format(file_size, type_count, i), 'w') as f:
            f.write(content)
                    
    # size static, and show effect of type 
    
def size_gen_dataset(batch_size=50):

    # type static
    pass
def rand_gen_dataset(batch_size=50):
    # learn the params 
    pass
def gen():
    for i in range(30, 255, 10):
        for j in range(1, 1001, 20):
            gen_dataset(type_count=i, file_size=j * 1024)
gen_image()
# 使用一个公式来找到最适用于哈夫曼 的压缩参数。评估效率，找到最优的大小与压缩比和时间

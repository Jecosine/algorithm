#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
int main()
{
    int n;
    double a[70],Hanoi[70],k1,k2;
    int i,j;
    for(i = 1;i <= 64;i++)
        a[i] = pow(2,i) - 1;
    Hanoi[1] = 1;
    for(i = 2;i <= 64;i++)
    {
        k1=a[i];
        for(j = 1;j < i;j++)
        {
            k2 = a[j] + 2*Hanoi[i-j];
            if(k1 > k2)
                k1 = k2;
        }
        Hanoi[i] = k1;
    }
    while(scanf("%d",&n)!=EOF)
    {
        printf("%.f\n",Hanoi[n]);
    }
    return 0;
}

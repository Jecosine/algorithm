#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
using namespace std;

int main() {
	int n;
	cin >> n;
	int a[n+2][n+2];
	memset(a, 0, sizeof(a));
	if(n == 1){
		cout <<'1'<<endl;
		return 0;
	}
	else {
		a[0][1] = 1;
	}
	for(int i=1;i<n;i++){
		for(int j = 1;j < i + 2;j++){
			a[i][j] = a[i-1][j-1]+a[i-1][j];
		}
	}
	for(int i=0;i<n;i++){
		for(int j = 1;j < i+2;j++){
			cout << a[i][j]<<" ";
		}
		cout << endl;
	}
	return 0;
}

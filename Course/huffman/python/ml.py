import tensorflow as tf
import numpy as np

def add_layer(inputs, in_size, out_size, activation_function = None):
    weights = tf.Variable(tf.random.normal([in_size, out_size]))
    biases =tf.Variable(tf.zeros([1, out_size]) + 0.1)
    result = tf.matmul(inputs, weights) + biases
    if activation_function is None:
        outputs = result
    else:
        output = activation_function(result)
x_data = np.linspace(-1, 1, 300)[:, np.newaxis]
noise = np.random.normal(0, 0.05, x_data.shape)
y_data = np.square(x_data) - 0.5
# xs = tf.placeholder(tf.float32, [None, 1])
# ys = tf.placeholder(tf.float32, [None, 1])
xs = tf.placeholder(tf.float64, [None,None], name="x")
ys = tf.placeholder(tf.float64, [None,], name='y')
# l1 
l1 = add_layer(xs, 4, 10, activation_function=tf.nn.relu)
prediction = add_layer(l1, 10, 1352,activation_function=None)

loss = tf.reduce_mean(tf.reduce_sum(tf.square(ys - prediction), reduction_indices=[1]))
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(loss)

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    for i in range(1000):
        sess.run(train_step, feed_dict = {xs: x_data, ys: y_data})
        if i % 50:
            sess.run(loss, feed_dict={xs: x_data, ys: y_data})

#include <cstdio>
#include <iostream>

using namespace std;
int main(){
	int n, r = 0;	
	cin >> n;
	int a[n], t = 0;
	for(int i = 0; i < n; t += a[i],i++)
		cin >> a[i];
	t = t / n;
	for(int i = 0; i < n - 1; i++)
		if(a[i] != t && ++r)
			a[i+1] -= t - a[i];
	cout << r;
	return 0;
}

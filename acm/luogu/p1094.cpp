#include <bits/stdc++.h>

using namespace std;
vector<int> arr;
int main(){
	int n, lm;
	cin >> lm >> n;
	arr.resize(n);
	for(int i = 0; i < n;i++)
		cin >> arr[i];
	sort(arr.begin(), arr.end());
	int len = arr.size(), t, c = 0;
	bool flag;
	while(len){
		flag = true;
		t = arr[len-1];
		arr.pop_back();
		len--;
		for(int i = len-1; i >= 0; i--)
			if(t + arr[i] <= lm){
				c++;
				arr.erase(arr.begin() + i);
				len--;
				flag = false;
				break;
			}
		if(flag)
			c++;
		
	}
	cout << c;
	return 0;
}

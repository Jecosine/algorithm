#include <bits/stdc++.h>
#define ll long long
using namespace std;
int r[31][31], a[31];
ll dp[31][31];
ll dfs(int b, int e){
  if(b > e) return 1;
  if(dp[b][e])
    return dp[b][e];
  ll mx = 0, t;

  for(int i = b; i < e; i++){
    t =  a[i] + dfs(b, i - 1) * dfs(i+1, e);
    if(t > mx){
      mx = t;
      r[b][e] = i;
    }
  }
  return dp[b][e] = mx;
}
void fb(int b, int e){
  if( b > e)
    return ;
  cout << r[b][e] + 1 << " ";
  fb(b, r[b][e]-1);
  fb(r[b][e] + 1, e);

}
int main(){
  int n;
  cin >> n;
  memset(dp, 0, sizeof(dp));
  for(int i =0;i < n; i++){
    cin >> a[i];
    dp[i][i] = a[i];
    r[i][i] = i;
  }
  cout << dfs(0, n-1) << endl;
  fb(0 ,n-1);
  return 0;
}
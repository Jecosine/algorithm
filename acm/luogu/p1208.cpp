#include <bits/stdc++.h>
using namespace std;
typedef struct {
	int p, q;
} P;
bool cmp(P p1, P p2){
	return p1.p < p2.p;
}
int main(){
	int n, m, t = 0;
	cin >> n >> m;
	P p[m];
	for(int i = 0; i < m; i++){
		cin >>p[i].p >> p[i].q;
	}
	sort(p, p + m, cmp);
	for(int i = 0; i < m; i++){
		if (n > p[i].q){
			n -= p[i].q;
			t += p[i].q * p[i].p; 
		} else{
			t += p[i].p * n;
			break;
		}
	}
	cout << t;
	return 0;
}

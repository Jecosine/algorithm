#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
bool judge(int y){
	return (y % 4 == 0 && y % 100 != 0) || (y % 400 == 0);
}
int main() {
	int y;
	cin >> y;
	cout << ((judge(y))?"yes":"no")<<endl;
	return 0;
}

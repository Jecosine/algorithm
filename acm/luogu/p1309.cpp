#include <bits/stdc++.h>
using namespace std;
typedef struct {
	int i;
	int p;
	int total;
} P ;
P p[200003];
bool cmp(P p1, P p2){
	if (p1.total == p2.total)
		return p1.i < p2.i;
	else 
		return p1.total > p2.total;
}
int main(){
	int n, r, q;
	scanf("%d%d%d", &n, &r, &q);
	int t = 2 * n;
	for(int i = 0; i < t; i++){
		p[i].i = i+1;
		scanf("%d", &p[i].total);		
	}
	for(int i = 0; i < t; i++){
		scanf("%d", &p[i].p);		
	}
	for(int i = 0; i < r; i++){
		sort(p, p+2*n, cmp);
		for(int j = 0; j < n; j++){
			if(p[2*j].p > p[2*j + 1].p)
				p[2*j].total++;
			else
				p[2*j+1].total++;
		}		
	}
	sort(p, p+2*n, cmp);
	
	printf("%d",p[q-1].i);
	
	return 0;
}

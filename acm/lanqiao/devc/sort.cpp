#include <iostream>
#include <cstdio>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;
void bubble(int a[], int n);
void select(int a[], int n);
void insert(int a[], int n);
int main() {
	int n;
	cin >> n;
	int a[n];
	for(int i = 0;i < n; i++)
		cin >> a[i];
	insert(a, n);	
	// out
	for(int i = 0;i<n;i++){
		cout << a[i] << " ";
	}
	return 0;
}

void select(int a[], int n){
	// select
	int t;
	for(int i = 0; i < n; i++){		
		t = a[i];
		for(int j = i; j < n; j++){
			if (a[j] < t){
				t = a[j];
				a[j] = a[i];
				a[i] = t;			
			}
		}		
		a[i]= t;
	}	
}
void insert(int a[], int n){
	// insert
	int t;
	for (int i = 0; i < n-1; i++){
		int j = i;
		while(j >= 0){
			if (a[j] > a[j+1]){			
				int t = a[j+1];
				a[j+1] = a[j];
				a[j] = t;
			}
			j--;
		}
	}
}
void bubble(int a[], int n){
	// bubble
	int t;
	for(int i = 0; i < n; i++){
		for(int j = n-1; j > i;j--){
			if (a[j] < a[j-1])
			{
				t = a[j];
				a[j] = a[j-1];
				a[j-1] = t;
			}
		}
	}
}

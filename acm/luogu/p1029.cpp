#include<bits/stdc++.h>
#define ll long long
using namespace std;
bool vis[100001];
void eratos(bool* vis, int n){
	for(int i = 2; i <= n;i++)
		if(!vis[i])
			for(int j = i * 2; j <= n; j+=i)
				vis[j] = true;
}
ll judge(ll a, ll b){
	return (!b)?a:judge(b,a%b);
}
int main(){
//	memset(vis, 0, sizeof(vis));
//	eratos(vis, 100000);
	ll p, q, s;
	int c = 0;
	cin >> p >> q;
	if (q % p != 0){
		cout << 0;
		return 0;
	}
	s = q/p;
	ll l = sqrt(s);
	for(int i = 1; i <= l; i ++){
		if(s % i == 0){
			if (judge(i, s/i)==1)
				c++;
		}
	}
	cout << ((p==q)?c:(c*2));
	return 0;
} 

class Node:
    def __init__(self,freq):
        self.left = None
        self.right = None
        self.father = None
        self.freq = freq
        self.obj = None
 
    def is_left(self):
        return self.father.left == self
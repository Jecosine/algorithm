#include <stdio.h>
void reverse(int *p, int length);
int main(){
    int a[10] = {1,2,3,4,5,6,7,8,9,10};
    reverse(a, 10);
    for(int i = 0;i < 10;i++){
        printf("%d ", a[i]);
    }
    return a;
}

void reverse(int *p, int length){
    int mid = length / 2;
    for(int i = 0; i < mid; i++){
        int t = *(p+i);
        *(p + i) = *(p + length - i - 1);
        *(p + length - i - 1) = t;
    }
}

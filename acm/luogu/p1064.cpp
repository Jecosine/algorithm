#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
typedef struct item {
	int i;
	int v;
	int w;
	item(int x, int y, int z){ i = x;v = y;w = z;}
};
using namespace std;
vector<item> mainitem;
vector<item> G[65];
int a,b,c;
int dp[32000], dt[181];
bool visited[60];

int n, m;
// tree dp 
// tree to serial https://www.cnblogs.com/colazcy/p/11514778.html
// 3 case https://blog.csdn.net/fighting_yifeng/article/details/81476595
// 5 case https://www.cnblogs.com/-xiangyang/p/9368477.html
// 4 condition compare this dp[60][4];
// group package 
// extraction this
int main(){
	int mainc = 0;
	cin >> n >> m;
	memset(dp, 0, sizeof(dp));
	for(int i=1; i <= m;i++){		
		cin >> a >> b >> c;
		b *= a;
		if(!c)
			mainitem.push_back(item(i, a, b));		
		else
			G[c].push_back(item(i, a, b));
	}
	mainc = mainitem.size();
	for(int i = 0 ; i < mainc; i++)
		for(int j = n; j >= mainitem[i].v;j--){
			int t = G[mainitem[i].i].size();
			dp[j] = max(dp[j], dp[j - mainitem[i].v] + mainitem[i].w);
			if (t == 1 && j >= mainitem[i].v + G[mainitem[i].i][0].v)
				dp[j] = max(dp[j], dp[j - mainitem[i].v - G[mainitem[i].i][0].v] + mainitem[i].w + G[mainitem[i].i][0].w);
			if(t == 2){
				if(j >= mainitem[i].v + G[mainitem[i].i][0].v)
					dp[j] = max(dp[j], dp[j - mainitem[i].v - G[mainitem[i].i][0].v] + mainitem[i].w + G[mainitem[i].i][0].w);
				if(j >= mainitem[i].v + G[mainitem[i].i][1].v)
					dp[j] = max(dp[j], dp[j - mainitem[i].v - G[mainitem[i].i][1].v] + mainitem[i].w + G[mainitem[i].i][1].w);
				if(j >= mainitem[i].v + G[mainitem[i].i][1].v + G[mainitem[i].i][0].v)
					dp[j] = max(dp[j], dp[j - mainitem[i].v - G[mainitem[i].i][0].v - G[mainitem[i].i][1].v] + mainitem[i].w + G[mainitem[i].i][1].w + G[mainitem[i].i][0].w);
			}
			//int t = dp[j];
			 
			//if(dp[j] != t && n == j)
			//	cout << "dn add item: " << i << " value: " << mainitem[i].v << " weight: " << mainitem[i].w << endl << "form: " << t << "   after: " << dp[j] << " j-v: "<< dp[j - mainitem[i].v] << endl;
		}	
	cout << dp[n];
	return 0;
}

#include <bits/stdc++.h>

using namespace std;
int n;
int partition(int *a, int low, int high){	
	int l = low, h = high, m;
	m = (rand() % (high-low+1)) + low;
	int t = a[low];
	a[low] = a[m];
	a[m] = t;
	m = a[low];
	while(low < high){
		while(low < high && a[high] >= m)
			high--;
		if(low < high){
			t = a[low];
			a[low] = a[high];
			a[high] = t;
		}
		while(low < high && a[low] <= m)
			low++;
		if(low < high){
			int t = a[low];
			a[low] = a[high];
			a[high] = t;
		}
	}
	//a[low] = m;
	return low; 
}
void qsort(int *a, int low, int high){
	if (low > high)
		return ;
	int m = partition(a, low, high);
	qsort(a, low, m - 1);
	qsort(a, m+1, high);
	
}
int main(){	
	scanf("%d", &n);
	int a[n+1];
	srand((int)time(0));
	for(int i = 0; i < n; i++)
		scanf("%d", &a[i]);
	qsort(a, 0, n-1);
	for(int i = 0; i < n; i++)
		printf("%d ", a[i]);
	return 0;
}

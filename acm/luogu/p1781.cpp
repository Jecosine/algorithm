#include <bits/stdc++.h>
using namespace std;
typedef struct {
	int id;
	string v;
} P;
bool cmp(P p1, P p2){
	if (p1.v.size() != p2.v.size())
		return p1.v.size() > p2.v.size();
	int len = p1.v.size();
	for(int i = 0; i < len; i++){
		if (p1.v[i] != p2.v[i])
			return p1.v[i]-48 > p2.v[i]-48;
	}
	return false;
}
P p[21];
int main(){
	int n;	
	cin >> n;
	for(int i = 1; i <= n; i++){
		p[i-1].id = i;
		cin >> p[i-1].v;
	}
	sort(p, p + n, cmp);
	for(int i = 0; i < n; i++){
		cout << p[i].id << " " << p[i].v << endl;
	}
	cout << p[0].id << endl << p[0].v << endl;	
	return 0;
}

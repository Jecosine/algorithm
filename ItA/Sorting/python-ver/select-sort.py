#Select-Sort
def select_sort(increase = True):
	s = raw_input()
	s = [int(item) for item in s.split(' ')]
	a = [i for i in range(len(s))]
	temp = s[0]
	for i in range(len(s)-1):
		for j in range(i+1,len(s)):
			if s[j] <= temp:
				temp, s[j] = s[j], temp	
		a[i] = temp
		temp = s[i+1]
	a[-1] = s[-1]
	#Format output
	if increase:
		for i in a:
			print i,
	else:
		for i in range(len(a)):
			print a[len(a)-i],
			

select_sort()
		

#include <bits/stdc++.h>
using namespace std;
int dp[31][31];
int main(){
	memset(dp, 0, sizeof(dp));
	int n, m;
	cin >> n >> m;
	dp[1][0] = dp[2][1] = dp[n][1] = 1;
	
	for(int j = 1; j <= m; j++){
		for(int i = 1; i <= n; i++){
			if(i == n)
				dp[i][j] = dp[i-1][j-1] + dp[1][j-1];
			else if (i == 1)
				dp[i][j] = dp[i+1][j-1] + dp[n][j-1];
			else dp[i][j] = dp[i-1][j-1] + dp[i+1][j-1];
		}
	}
	cout << dp[1][m];
	return 0;
}

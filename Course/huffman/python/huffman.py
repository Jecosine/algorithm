import os
import pickle
from utils import *
import uuid
import math
import time

class HuffmanEncoder:
    def __init__(self, text, bin_mode = False):
        self.origin = text
        self.content_length = len(text)
        self.bin_mode = bin_mode
        # time calc
        self.encode_time = 0
        self.decode_time = 0
        self.char = []
        self.freq = []
    def test(self):
        t1 = time.time()
        self.count_frequency()
        self.get_char_frequency()
        self.create_nodes()
        self.build_tree()
        self.huffman_encoding()
        self.get_huffman_file_binary()
        self.encode_time = time.time() - t1
        t1 = time.time()
        self.decode_huffman_bin()
        self.decode_time = t1 - time.time()
        
    def retrieve(self):
        pass
    def _encode(self):
        t1 = time.time()
        self.count_frequency()
        self.get_char_frequency()
        self.create_nodes()
        self.build_tree()
        self.huffman_encoding()
        self.get_huffman_file_binary()
        self.encode_time = time.time() - t1
    def _encode_test(self):
        t1 = time.time()
        self.count_frequency()
        self.get_char_frequency()
        self.create_nodes()
        self.build_tree()
        self.huffman_encoding()
        self.get_huffman_file_test()
        self.encode_time = time.time() - t1
    def _decode(self):
        t1 = time.time()
        self.decode_huffman_bin()
        self.decode_time = time.time() - t1
    def _decode_test(self):
        t1 = time.time()
        self.decode_huffman_test()
        self.decode_time = time.time() - t1
    def count_frequency(self):
        self.char = []
        self.freq = []
        for index in range(len(self.origin)):
            if self.char.count(self.origin[index]) > 0:
                temp = int(self.freq[self.char.index(self.origin[index])])
                temp = temp + 1
                self.freq[self.char.index(self.origin[index])] = temp
            else:
                self.char.append(self.origin[index])
                self.freq.append(1)

    def get_char_frequency(self):
        self.char_frequency = []
        for item in zip(self.char, self.freq):
            temp = (item[0], item[1])
            self.char_frequency.append(temp)       

    def create_nodes(self):
        self.nodes = [Node(freq) for freq in self.freq] 
        for i in range(len(self.nodes)):
            self.nodes[i].obj = self.char[i]
    def select(self, queue, n):
        node_left = node_riuht = -1
        m1 = m2 = math.inf
        for i in range(n):
            if queue[i].father:
                continue
            if queue[i].freq < m2:
                if queue[i].freq < m1:
                    node_right = node_left
                    node_left = i
                    m2 = m1
                    m1 = queue[i].freq
                else:
                    m2 - queue[i].freq
                    node_right = i
        return node_left, node_right
    def build_tree(self):
        queue = self.nodes[:]
        queue += [Node(0) for i in range(len(self.nodes) - 1)]
        for i in range(len(self.nodes), len(self.nodes)*2 - 1):            
            #queue.sort(key=lambda item: item.freq)
            l, r = self.select(queue, i)

            #print(l, r)
            queue[l].father = queue[i]
            queue[r].father = queue[i]
            queue[i].left = queue[l]
            queue[i].right = queue[r]
            queue[i].freq = queue[l].freq + queue[r].freq
            # node_left = queue[l] if l >= 0 else None
            # node_right = queue[r] if r >= 0 else None
            # node_father = Node(node_left.freq + node_right.freq)
            # node_father.left = node_left
            # node_father.right = node_right
            # node_left.father = node_father
            # node_right.father = node_father
            # queue.append(node_father)
        #queue[-1].father = None
        self.root = queue[-1]
        p = self.root.father
        while p:
            self.root = p
            p = self.root.father

        self.total_nodes = queue
        return queue[0]
    def search_tree(self):
        queue = self.nodes[:]
        self.root = queue[0]
        cur = self.root
        while cur.left and cur.right:
            pass
    def huffman_encoding(self):
        codes = [''] * len(self.nodes)
        for i in range(len(self.nodes)):
            node_tmp = self.nodes[i]
            while node_tmp != self.root:
                if node_tmp.is_left():
                    codes[i] = '0' + codes[i]
                else:
                    codes[i] = '1' + codes[i]
                node_tmp = node_tmp.father
        self.codes = codes
    
    def get_huffman_file(self):
        file_content = ''
        for index in range(len(self.origin)):
            for item in zip(self.char_frequency, self.codes):
                if self.origin[index] == item[0][0]:
                    #print(index)
                    file_content = file_content + item[1]
        file_name = 'huffman_' + str(uuid.uuid1())+'.txt'
        with open(file_name, 'wb') as f:
            f.write(bytes(file_content.encode("utf-8")))
        return file_name

    def get_huffman_file_binary(self):
        file_content = ''
        for index in range(len(self.origin)):
            for item in zip(self.char_frequency, self.codes):
                if self.origin[index] == item[0][0]:
                    file_content = file_content + item[1]
        # self.encoded = file_content
        file_name = "encoded.data"
        # 0 fill 
        # self.fill_count = 8 - len(file_content) % 8
        # file_content += self.fill_count * "0"
        bytes_content = []
        
        for i in range(0, len(file_content), 8):
            bytes_content.append(int(file_content[i: i + 8 if i + 8 <= len(file_content) else len(file_content)], 2))
        #print(bytes_content)
        # saving tree
        # head = bytes(str(self.codes).encode('utf-8'))
          
        with open(file_name, 'wb') as f:
            # f.write(bytes([len(head)]))
            # f.write(head)
            f.write(bytes(bytes_content))
        #self.bytes_content = bytes_content
        self.file_content = file_content
        return file_name
    
    def get_huffman_file_test(self):
        file_content = ''
        for index in range(len(self.origin)):
            for item in zip(self.char_frequency, self.codes):
                if self.origin[index] == item[0][0]:
                    file_content = file_content + item[1]
        # self.encoded = file_content
        file_name = "encoded.data"
        # 0 fill 
        # self.fill_count = 8 - len(file_content) % 8
        # file_content += self.fill_count * "0"
        bytes_content = []
        self.bytes_count = len(file_content) // 8
        self.file_content = file_content
        #for i in range(0, len(file_content), 8):
        #   bytes_content.append(int(file_content[i: i + 8 if i + 8 <= len(file_content) else len(file_content)], 2))
    def decode_huffman_bin(self):
        #encode = self.bytes_content
        decode = []
        i = 0
        p = self.root
        for i in range(len(self.file_content)):
            if (not p.left) and (not p.right):
                decode.append(p.obj)
                p = self.root
            if self.file_content[i] == '1':
                p = p.right
            elif self.file_content[i] == '0':
                p = p.left

        # with open("3.png",'wb') as f:
        #     f.write(bytes(decode))
    def decode_huffman_test(self):
        i = 0
        p = self.root
        for i in range(len(self.file_content)):
            if (not p.left) and (not p.right):
                p = self.root
            if self.file_content[i] == '1':
                p = p.right
            elif self.file_content[i] == '0':
                p = p.left

        
    def decode_huffman(self):
        encode = self.bytes_content
        decode = ''
        i = 0
        p = self.root
        while i < len(encode):
            buff = encode[i: i+8 if i + 8 <= len(encode) else len(encode)]
            buff = [bin(b)[2:] for b in buff]
            buff = ''.join(buff)
            for index in range(len(buff)):
                if buff[index] == '1':
                    p = p.right
                else:
                    p = p.left
                if (not p.left) and (not p.right):
                    decode += p.obj
                    p = self.root
            i += 8
        return decode
if __name__ == "__main__":
    with open("2.png",'rb') as f:
        text = f.read()
    
    h = HuffmanEncoder(text, True)
    h._encode()
    h._decode()
    print(h.encode_time, h.decode_time)
    # h.decode_huffman_bin()
    
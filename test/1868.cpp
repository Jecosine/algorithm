#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;
int main(){
    vector<int> quene;
    int n, sum = 0, c = 0;
    while(scanf("%d", &n) != EOF){
        int i  = 1;
        while(i < n / 2 + 2 && i < n){
            if (sum < n){
                quene.push_back(i);
                sum += i;
                i++;
            }

            if (sum > n){
                while(sum > n){
                    sum -= quene[0];
                    quene.erase(quene.begin());
                }
            }
            if (sum == n){
                sum -= quene[0];
                quene.erase(quene.begin());
                c++;
            }
            for(int j = 0; j < quene.size(); j++)
                    printf("%d ", quene[j]);
            printf("sum: %d\n", sum);
        }
        printf("%d\n", c);
        c = 0;
        sum = 0;
        quene.erase(quene.begin(), quene.end());
    }
    return 0;
}

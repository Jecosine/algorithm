#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

typedef struct Test{
	int a = 0;
	Test* next = NULL;
} *TNode;


int main(){
	Test a[10];
	ofstream out("test.data", ios::out);
	char buffer[256];
	char *p = buffer;
	int b = 99;
	a[5].a=9;
	cout<< sizeof(a)<< " " <<sizeof(Test)<<endl;
	memcpy(p, &a, sizeof(a));
	memcpy(p + sizeof(a) + 1, &b, sizeof(int)); 
	out.write(buffer, sizeof(a)+1 + sizeof(b));
	out.close();
		
	ifstream infile("test.data", ios::in);
	char ibuffer[256];
	infile.get(ibuffer, sizeof(a) + sizeof(b) + 1);
	Test ia[10];
	int c;
	memcpy(&ia, ibuffer, sizeof(ia));
	memcpy(&c, ibuffer + sizeof(ia)+1, sizeof(int));
	printf("%d %d", ia[5].a, c);
	return 0;
}

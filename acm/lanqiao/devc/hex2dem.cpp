#include <iostream>
#include <cstdio>

using namespace std;

int main() {
	string s;
	getline(cin, s);
	int l = s.size();
	long long t = 0;
	for(int i = 0;i < l; i++){
		t += (s[i] >= 'A')?s[i]-'A'+10:s[i]-'0';
		if(i+1 < l)
			t <<= 4;
	}	
	cout << t <<endl;
	return 0;
}

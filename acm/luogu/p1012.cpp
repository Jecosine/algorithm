#include <bits/stdc++.h>
using namespace std;
bool cmp (string s1, string s2){
	int l = (s1.size() > s2.size())?s1.size():s2.size(); 
	
	for(int i = 0; i < l; i++){
		
		if(s1[i%s1.size()]!=s2[i%s2.size()])
			return s1[i%s1.size()] > s2[i%s2.size()];
	}
	
	return s1.size()<s2.size();
} 
int main(){
	int n;
	cin >> n;
	string s[n];
	for(int i = 0 ; i < n; i++){
		cin >> s[i];
	}
	sort(s, s+n, cmp);
	for(int i = 0; i < n; i++)
		cout << s[i];
	return 0;
} 

#include <stdio.h>
//do while implement of adding from 1 to 100
int main(){
    int i = 1, s = 0;
    do{
        s += i++;
    }
    while(i <= 100);
    printf("%d", s);
    return 0;
}

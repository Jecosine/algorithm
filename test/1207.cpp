#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>


using namespace std;
int main(){
    double h[70] = { 0 }, ha[70] = {1, 1};
    for(int i = 1; i < 65; i++){
        h[i] = pow(2, i) - 1;
    }
    double k, k1;
    for(int i = 2; i < 65; i ++){
        k = h[i];
        for(int j = 1; j < i; j++){
            k1 = ha[i - j] * 2 + h[j];
            if (k > k1){
                k = k1;
            }
        }
        ha[i] = k;
    }
    int n;
    while(cin >> n)
        printf("%.f\n", ha[n]);
    return 0;
}

#include<bits/stdc++.h>
using namespace std;
int main(){
	int n;
	cin >> n;
	int a[n], s = 0, mx = 0;
	for(int i = 0; i < n; i ++) cin >> a[i];
	s = mx = a[0];
	for(int i = 1; i < n; i ++){
		
		int t = s + a[i];
		if(t > a[i])
			s = t;
		else
			s = a[i];
		if(s > mx){
			mx = s;
		}
	}
	cout << mx;
	return 0;
}

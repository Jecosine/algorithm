#include <bits/stdc++.h>
using namespace std;
int a[1000001];
int main(){
	memset(a, 0, sizeof(a));
	int n, k;
	cin >> n >> k;
	a[0] = a[1] = 1;
	for(int i = 2; i <= n; i++){
		if (i <= k)
			a[i] = (2 * a[i-1]) % 100003;
		else{
			a[i] = 2 * a[i - 1] - a[i - k - 1];
			a[i] %= 100003;
		}
		
	}
	cout << ((a[n]<0)?(a[n] + 100003):a[n])<<endl;
	
	return 0;
} 

#include <bits/stdc++.h>
using namespace std;

string hadd(string a, string b){
  // zheng xu
  int t = 0;
  string c = "";
  if (a.size() < b.size()){
    c = a;
    a = b;
    b = c;
  } 
  c = "";
  int la = a.size(), lb = b.size();
  for(int i = 1; i <= la; i++){
    t = t + a[la-i] - 48 + ((i <= lb)? b[lb - i] - 48 : 0);
    c = (char)( t % 10 + 48 ) + c; 
    t /= 10;
  }
  if (t)
    c = (char)(t+48) + c;
  return c;
}

int main(){
  string a, b;
  cin >> a >> b;
  cout << hadd(a, b);
  return 0;
}
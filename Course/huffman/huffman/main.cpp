#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <map>
#include <vector>
#include <bitset>
using namespace std;
typedef struct Tree {
    int w = 0;
    int l = 0, r = 0, p = 0;
} *TNode;

typedef struct CharType{
    char e;
    int w;
};
// typedef char** CodeList;

class Huffman{
public:
    int type_count = 0;
    int *weights;
    map<char, int> weight_map, char_map;
    vector<char> quene;
    vector<string> codelist;
    TNode tree = NULL;
    TNode root = NULL;
    string origin_text, encoded_text;
    char** ot;
    //CodeList codelist = NULL;
    Huffman(){

    };
    void increase_weight_bin(char c){
        map<char, int>::iterator iter = weight_map.find(c);
        bitset<8> bits;
        bits = c;
        if (iter != weight_map.end())
            iter->second++;
        else{
            weight_map[c] = 1;
            quene.push_back(c);
        }

    };
    void increase_weight(unsigned char c){
        map<char, int>::iterator iter = weight_map.find(c);

        if (iter != weight_map.end())
            iter->second++;
        else{
            type_count += 1;
            //cout << type_count << endl;
            weight_map[c] = 1;
            quene.push_back(c);
        }
    };
    void print_weight(){
        cout << weight_map.size()<<endl;
        for(map<char, int>::iterator i = weight_map.begin(); i != weight_map.end(); i++)
            printf("%c -> %d\n", i->first, i->second);
            //cout << i->first << ":" << i->second << endl;
    };
    void clear_weight(){
        weight_map.erase(weight_map.begin(), weight_map.end());
    };
    void read_text_origin(char* fname){
        ifstream infile(fname, ios::in);
        while(!infile.eof()){
            char buffer[128];
            infile.get(buffer, 128);
            char *p = buffer;
            while (*p != '\0'){
                increase_weight(*p);
                origin_text = origin_text + *p;
                p++;
            }
        }
        for(int i=0;i < quene.size();i++){
            char_map[quene[i]] = i;

        }
        print_weight();
        type_count = weight_map.size();
    };
    void read_bin_origin(char* fname){
        ifstream infile(fname, ios::in|ios::binary);
        char c[1024];
        char *p = c;
        int t = 0;
        bitset<8> bits;
        while(!infile.eof()){
            t++;
            infile.read(c, 1024);
            p = c;
            //increase_weight(c);
//            ot + t = (char*)malloc(1024);
//            memcpy(ot+t, c, 1024);
            while (*p != EOF){
                bits = (*p);
                increase_weight(bits.to_ulong());
//                cout << bits << " ";
                origin_text = origin_text + *p;
                p++;
            }
            //origin_text = origin_text + c;
        }
        //for(int i=0;i < quene.size();i++){
        //    char_map[quene[i]] = i;
        //}
        //cout << origin_text<<endl;
        print_weight();
        type_count = weight_map.size();
        cout << type_count << endl;
    };
    void read_text_console(){
        string content, temp;
        while(getline(cin, temp))
            content += temp;

        for(int i = 0; i < content.size(); i++){
            if (content[i] != '\0' && content[i] != EOF){
                increase_weight(content[i]);
            }
        }
        for(int i=0;i < quene.size();i++)
            char_map[quene[i]] = i;
        print_weight();
        type_count = weight_map.size();
    };
    void select_node(TNode &t, int s, int& i1, int &i2){
        int min1 = RAND_MAX, min2 = RAND_MAX;
        i1 = i2 = -1;
        for(int i = 1; i <= s; i++){
            if(t[i].p != 0)
                continue;
            if(t[i].w < min2){
                if(t[i].w < min1){
                    min2 = min1;
                    min1 = t[i].w;
                    i2 = i1;
                    i1 = i;
                }
                else {
                    min2 = t[i].w;
                    i2 = i;
                }
            }
        }
    };
    void build_tree(){
        int c = type_count * 2;
        cout << type_count;
        int i; TNode p;
        tree = new Tree[c];
        for(i = 1, p = tree; i <= type_count; i++, p++){
            tree[i].w = weight_map[quene[i-1]];
        }
        int i1 = -1, i2 = -1;
        for(;i < c; i++){
            select_node(tree, i-1, i1, i2);
            tree[i1].p = i;
            tree[i2].p = i;
            tree[i].l = i1;
            tree[i].r = i2;
            tree[i].w = tree[i1].w + tree[i2].w;
        }
//        for(int i = 0; i < type_count*2; i++){
//            printf("%d l: %d;r: %d;p: %d;w: %d\n",i,tree[i].l, tree[i].r,tree[i].p, tree[i].w);
//        }
    };
    void encode_from_tree(){
        string temp = "";
        for(int i = 1;i < type_count+1;i++){

            TNode p = NULL;
            //printf("%c  ", quene[i]);
            for(int j = i, p = tree[i].p; p != 0; j = p, p = tree[p].p){
                //printf("%d -> ", j);
                if(j == tree[p].l)
                    temp.insert(0, "0");
                else
                    temp.insert(0, "1");
            }
            //cout<< endl;
            codelist.push_back(temp);
            temp.erase(temp.begin(), temp.end());
        }
        for(int i = 0; i < codelist.size();i++)
            printf("%c ----- %s \n",quene[i], codelist[i].c_str());
            //cout << quene[i] << "   " << codelist[i] << endl;
        encoded_text = "";
        for(int i = 0; i < origin_text.size(); i++){
            cout << i << " "<< char_map[origin_text[i]]<< " " << origin_text[i] << " " << codelist[char_map[origin_text[i]]]<<endl;
            encoded_text += codelist[char_map[origin_text[i]]];
        }
        //cout << "prpr" << encoded_text << endl;
    };
    void write_encode(char* fname){

        //ofstream out(fname, ios::out|ios::binary);
        //out << "HUFFMAN";
        FILE *out = fopen(fname, "wb");
        Tree data[type_count*2];

        for(int i = 0; i < type_count*2; i++){
            data[i].l = tree[i].l;data[i].r = tree[i].r;data[i].p = tree[i].p;data[i].w = tree[i].w;
        }
        for(int i = 0; i < type_count*2; i++){
            fprintf(out, "%d %d %d %d", data[i].l, data[i].r, data[i].p, data[i].w);
        }
        //char buffer[sizeof(data)];
//        int bf_size = sizeof(int) + sizeof(data);
        //memcpy(buffer, data, sizeof(data));
        //memcpy(buffer, &type_count, sizeof(int));
//        //out << type_count;

//        memcpy(buffer, data, sizeof(data));
        //cout << sizeof(tree) << endl;
        //out.write(buffer, sizeof(data));
        //out.write(buffer, sizeof(data));
        for(int i = 0; i < type_count;i++){
            fprintf(out, "%c",quene[i]);
        }
        //for(int i = 0; i < type_count*2; i++){
              //  printf("%d l: %d;r: %d;p: %d;w: %d\n",i,data[i].l, data[i].r,data[i].p, data[i].w);
        //}
        //printf("pre :%s\n", encoded_text.c_str());
        bitset<8> bits;
        while(encoded_text.size() >= 8){
            bits = bitset<8>(encoded_text, 0, 8);
            fprintf(out, "%c", static_cast<char>(bits.to_ulong()));
            //cout <<bits.to_string()<<endl;
            encoded_text.erase(0, 8);
        }
        unsigned long loc = encoded_text.size();
        bits = bitset<8>(encoded_text,0, loc);
        fprintf(out, "%c",static_cast<char>(bits.to_ulong()));
        //cout <<bits.to_string();
        fprintf(out,"%c", static_cast<char>(loc));
        bits = loc;
        //delete bits;
        if(encoded_text.size() > 0){

            //cout << bits.to_string()<< endl;
        }
        fclose();
        //printf("size: %d %d", sizeof(data) ,sizeof(Tree));

    };
    void encode(char* fname){
        read_text_origin(fname);
        build_tree();
        encode_from_tree();
        write_encode("encoded.data");
    };
    void decode(){
        if (codelist.size() == 0){
            printf("Code list has not been initialize.");
            return;
        }



    };
//    void rebuild
    void read_encoded_file(char* fname){
        ifstream infile(fname, ios::in|ios::binary);
//        string pattern = "HUFFMAN";
//        printf("\nReading File\n");
//        char head[8];
//        infile.get(head, 8);
//        cout << head << endl;
//        //delete head;
//        for(int i = 0; i < 7 &&!infile.eof(); i++){
//           // cout << pattern[i] << " " << head[i] << endl;
//            if(pattern[i] != head[i]){
//                printf("\nNot a HFM file\n");
//                return;
//            }
//        }
        Tree *data;
        char c;
        int bf_size;
        // read the fucking stucture and size from fucking memory
        if(!infile.eof()){
//            char head[sizeof(int)];
//            infile.get(head, sizeof(int));
//            memcpy(&type_count, head, sizeof(int));
            //infile >> type_count;
            //cout << type_count<<endl;
            bf_size = (type_count*2)*sizeof(Tree);
            //printf("\n   %d   \n", bf_size);
            data = (TNode)malloc(bf_size);
            //printf("Get %d type of char\n", type_count);
            char buffer[bf_size];

            infile.get(buffer, bf_size);
            memcpy(data, buffer, bf_size);
            for(int i = 0; i < type_count*2; i++){
                printf("%d l: %d;r: %d;p: %d;w: %d\n",i,data[i].l, data[i].r,data[i].p, data[i].w);
            }

        }
        infile.seekg(1, ios::cur);
        for(int i = 0; i < type_count;i++){
            infile >> quene[i];
            //cout << quene[i];
        }
        //cout <<endl;
        bitset<8> bits;
        // deal with tail

        //infile.seekg(1, ios::cur);
        encoded_text = "";
        while(!infile.eof()){
            infile.read(&c, 1);
            bits = c;
            //cout << bits.to_string();
            encoded_text += bits.to_string();

        }
        infile.close();

        //cout << endl;
        // deal with end
        string e = encoded_text.substr(encoded_text.size() - 24, 16);
        bitset<8> loc(e, 8, 16);
        unsigned add = loc.to_ulong();
        e = e.substr(8 - add, add);
        encoded_text.erase(encoded_text.size()-24, encoded_text.size());
        encoded_text += e;
        //cout << encoded_text<<endl;
        // get root
        root = &data[1];
        int root_index = 0;


        while(root->p != 0){
            if (root->p!= 0)
                root_index = root->p;
            root = &data[root->p];
        }
        int index = root_index;
        TNode p = root;
        //cout<<root_index<<endl;
        ofstream out("out.png", ios::out|ios::binary);
        for (int i = 0; i < encoded_text.size();i++){
            //cout << "OriginL:" << encoded_text[i] << "   ";

            if (encoded_text[i] == '0'){
                index = p->l;
                p = &tree[p->l];

            }

            if (encoded_text[i] == '1'){
                index = p->r;
                p = &tree[p->r];

            }

            //cout << index << endl;
            if (p->l == 0 && p->r == 0){
                //cout << "i " <<index;
                if(index != 0)
                loc = quene[index-1];
                out<<static_cast<char>(loc.to_ulong());
                p = root;
                index = root_index;
            }
            //cout << "#####";

        }
            //char buffer[128];
//            infile.get(buffer, 128);
//            char *p = buffer;
//            while (*p != '\0'){
//                increase_weight(*p);
//                p++;
//            }
//        }
        //print_weight();
        out.close();

        type_count = weight_map.size();

    };

    void read_bin_encoded_file(char* fname){
        ifstream infile(fname, ios::in|ios::binary);
        ofstream out("origin", ios::out | ios::binary);
        string pattern = "HUFFMAN";
        printf("\nReading File\n");
        char head[8];
        infile.get(head, 8);
        cout << head << endl;
        for(int i = 0; i < 7 &&!infile.eof(); i++){
           // cout << pattern[i] << " " << head[i] << endl;
            if(pattern[i] != head[i]){
                printf("\nNot a HFM file\n");
                return;
            }
        }
        Tree data[10];
        // read the fucking stucture and size from fucking memory
        if(!infile.eof()){
            int bf_size;
//            char head[sizeof(int)];
//            infile.get(head, sizeof(int));
//            memcpy(&type_count, head, sizeof(int));
            infile >> type_count;
            bf_size = (type_count*2)*sizeof(Tree);
            printf("Get %d type of char\n", type_count);
            char buffer[bf_size];
            infile.get(buffer, bf_size);
            memcpy(data, buffer, bf_size);
            for(int i = 0; i < type_count*2; i++){
                printf("%d l: %d;r: %d;p: %d;w: %d\n",i,data[i].l, data[i].r,data[i].p, data[i].w);
            }

        }
        infile.seekg(1, ios::cur);
        for(int i = 0; i < type_count;i++){
            infile >> quene[i];
            //cout << quene[i];
        }
        cout <<endl;
        char c;
        bitset<8> bits;
        // deal with tail

        //infile.seekg(1, ios::cur);
        encoded_text = "";
        while(!infile.eof()){

            infile.read(&c, 1);
            bits = c;
            //cout << bits.to_string();
            encoded_text += c;

        }
        // deal with end
        string e = encoded_text.substr(encoded_text.size() - 24, 16);
        bitset<8> loc(e, 8, 16);
        unsigned add = loc.to_ulong();
        e = e.substr(8 - add, add);
        encoded_text.erase(encoded_text.size()-24, encoded_text.size());
        encoded_text += e;
        cout << encoded_text<<endl;
        infile.close();
        // get root
        delete &loc;
        delete (&e);
        delete &add;
        root = &data[1];
        int root_index = 0;


        while(root->p != 0){
            if (root->p!= 0)
                root_index = root->p;
            root = &data[root->p];
        }
        int index = root_index;
        TNode p = root;
        //cout<<root_index<<endl;
        for (int i = 0; i < encoded_text.size();i++){
            //cout << "OriginL:" << encoded_text[i] << "   ";

            if (encoded_text[i] == '0'){
                index = p->l;
                p = &tree[p->l];

            }

            if (encoded_text[i] == '1'){
                index = p->r;
                p = &tree[p->r];

            }

            //cout << index << endl;
            if (p->l == 0 && p->r == 0){
                //cout << "i " <<index;
                if(index != 0){
                    bitset<8> bits = quene[index-1];
                    out << static_cast<char>(bits.to_ulong());
                }
                p = root;
                index = root_index;
            }
            out.close();
            //cout << "#####";

        }
            //char buffer[128];
//            infile.get(buffer, 128);
//            char *p = buffer;
//            while (*p != '\0'){
//                increase_weight(*p);
//                p++;
//            }
//        }
        print_weight();
        type_count = weight_map.size();

    };

};


int main(){

    Huffman h;
    h.read_bin_origin("2.png");
    h.build_tree();
    h.encode_from_tree();
    h.write_encode("encoded.data");
    h.read_encoded_file("encoded.data");
    //h.read_text_console();
    return 0;
}

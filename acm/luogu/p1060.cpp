#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <vector>
using namespace std;
int f[30001], cost[25], rank[25];
int main(){
	int n, m;
	cin >> n >> m;
	for(int i = 0; i<m;i++){
		cin >> cost[i] >> rank[i];
		rank[i] *= cost[i];		
	}
	for(int i = 0; i < m; i++)
		for(int j = n; j >= cost[i]; j--)
			f[j] = max(f[j - cost[i]] + rank[i], f[j]);
	cout << f[n];
	return 0;
}

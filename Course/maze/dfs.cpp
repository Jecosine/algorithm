#include <cstdio>
#include <cstdlib>
#include <vector>

class Point {
public:
    int r;
    int c;
    Point(int x, int y){
        r = x;
        c = y;
    }
    Point move(int dir){
        switch(dir){
            case 0:
                return Point(r, c + 1);
            case 1:
                return Point(r + 1, c);
            case 2:
                return Point(r, c - 1);
            case 3:
                return Point(r - 1, c);
            default:
                return Point(r, c);
        }
    }
}
class Path {
public:
    Point *head, *tail;
    int id;
    int length;
    vector<Point> stack;
    Path(int i){
        id = i;
        head = tail = NULL;
        length = 0;
    }
    Path(int i, Path &p){
        for(auto i:p){
            stack.push_back(i);
        }
        length = p.length;
    }
    void add(Point &p){
        stack.push_back(p);
        length++;
    }
    void print(Maze m){
        for(int i = 0; i < length;i++)
            m.m[stack[i].r][stack[i].c] = 2;
        for(int i = 0; i < m.rows; i++){
            for(int j = 0;j < m.cols; j++)
                printf(m.m[i][j]);
            printf("\n");
        }
    }
}
class Maze{
public:
    int rows, cols;
    int m[][], d[][];
    vector<Path> paths;
    Maze(int rows, int cols){
        this->rows = rows;
        this->cols = cols;
        this->m = new int[rows][cols];
        this->d = new int[rows][cols];
        Path *p = new Path(0);
        p->add(Point(0,0));
        paths.push_back(p);
    }
    void initMaze(){
        for(int i = 0; i < rows;i++)
            for(int j = 0;j < cols;j++)
                m[i][j] = 0;
    }
    bool valid(Point p){
        if(p.r < 0 || p.r > rows - 1 || p.c < 0 || p.c > cols - 1)
            return false;
        return true;
    }
    int bfs(int r, int c){
        Point current = *(new Point(r, c));
        // Path p = paths.back();
        //定义一个队列，从back入队，从front出队
        vector<Point> queue;
        // 起点入队
        queue.push_back(current);
        while(queue.size()){
            // 出队一个元素p
            Point p = queue.front();
            queue.pop_front();
            if(r == M.r - 1 && c == M.c - 1){
                //找到了一条路径，打印出来
            }
            // 判断是否可以右移
            if(valid(current.move(0))&&d[current.r][current.c]==0){
                Point temp = current.move(0);
                queue.push_back(temp);
                // 移动到temp的步数为到current的步数+1
                d[temp.r][temp.c] = d[current.r][current.c]+1;
            }
            // 判断是否可以下移
            if(valid(current.move(1))&&d[current.r][current.c]==0){
                Point temp = current.move(1);
                queue.push_back(temp);
                
                d[temp.r][temp.c] = d[current.r][current.c]+1;
            }
            // 判断是否可以左移
            if(valid(current.move(2))&&d[current.r][current.c]==0){
                Point temp = current.move(2);
                queue.push_back(temp);
                d[temp.r][temp.c] = d[current.r][current.c]+1;
            }
            // 判断是否可以上移
            if(valid(current.move(3))&&d[current.r][current.c]==0){
                Point temp = current.move(3);
                queue.push_back(temp);
                d[temp.r][temp.c] = d[current.r][current.c]+1;
            }                   
        }
    }
}
int dfs(Maze &M, int r, int c){
    int route_id = 0;
    if(r == M.r - 1 && c == M.c - 1)
        return 
}

int main(){

}
#include <bits/stdc++.h>


using namespace std;
int vis[1000001];
void eratos(int n){
	for(int i = 2; i <= n;i++)
			for(int j = i*2; j <= n; j+=i)
				vis[j] += 1;
}

int main(){
	memset(vis, 0, sizeof(vis));
	int n;
	cin >> n;
	eratos(n);
	vis[1] = -1;
	long long s = 0;
	for(int i = 1; i <= n; i++){
//		cout <<i <<" " << vis[i] + 2 << endl;
		s += vis[i] + 2;
	}
	cout << s;
	return 0;
} 

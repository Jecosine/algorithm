#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;
int f[1000002] = {0,1,1,0};
int fibo(int n){
//    if (n==1 || n == 2) {
//        return 1;
//    }
    if (f[n] != 0)
        return f[n];
    f[n] = (fibo(n-2) + fibo(n-1)) % 10007;
    return f[n];
}

int main(){
    int n;
    cin >> n;
    cout << f[n] << n<<endl;
    cout << fibo(n);
    return 0;
}

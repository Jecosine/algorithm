import os
import csv
from huffman import HuffmanEncoder
TXT_DATA_DIR = 'data/bin_data/'
file_list = []

def parse_filename(name):
    name = name.split('.')[0]
    name = name.split('-')
    return name

def test_batch():
    global file_list
    #cf = open("test.csv",'w',newline='')
    #writer = csv.writer(cf,dialect = "excel")
    #writer.writerow(['txt_size','type_count','encode_time','decode_time','compress_rate'])
    file_list = os.listdir(TXT_DATA_DIR)
    # file_list = [i for i in file_list if i.endswith('.txt')]
    file_list = [i for i in file_list]
    # record params: txt_size type_count encode_time decode_time compress_rate 
    row = []
    for i in file_list:
        with open(TXT_DATA_DIR + i, 'rb') as f:
            content = f.read()
        h = HuffmanEncoder(content,bin_mode = True)
        h._encode_test()
        h._decode_test()
        #txt_size, type_count, _ = parse_filename(i)
        row = [len(h.origin) / 1024, len(h.codes), h.encode_time, h.decode_time, h.bytes_count/int(len(h.origin))]
        #writer.writerow(row)
        print(i, row)

test_batch()



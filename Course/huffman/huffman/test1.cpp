#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <bitset>
using namespace std;

typedef struct Test{
	int a = 0;
	Test* next = NULL;
} *TNode;


int main(){
	Test a[10];
	ofstream out("test.data", ios::out|ios::binary);
	ofstream bout("test.bin", ios::out | ios::binary);
	char buffer[256];
	char *p = buffer;
	string s = "00001010";
	bitset<8> bits(s, 0 , 8);
	int b = 99;
	//memcpy(s, &a, sizeof(a));
	//memcpy(p + sizeof(a) + 1, &b, sizeof(int)); 
	out << static_cast<char>(bits.to_ulong());
	bout.write(s.c_str(),8);
	out.close();
	bout.close();
	ifstream infile("test.data", ios::in|ios::binary);
	char x;
	infile.read(&x, 1);
	printf("%d\n", x);
	bitset<8> bi = x;
	cout << bi.to_string();
	return 0;
}

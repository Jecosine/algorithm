#include <bits/stdc++.h>
using namespace std;
int e[11];
typedef struct {
	int i;
	int w;
	int d;
} P;
P p[20001];
bool cmp(P p1, P p2){
	if (p1.w != p2.w)
		return  p1.w > p2.w;
	else 
		return p1.i < p2.i;
}
int main(){
	int n, k;
	cin >> n >> k;
	for(int i = 0; i < 10; i++)
		cin >> e[i];
	for(int i = 1; i <= n; i++){
		p[i-1].d = i;
		p[i-1].i = i;
		cin >> p[i-1].w;
	}
	sort(p, p + n, cmp);
	for(int i = 1; i <= n; i++){
		p[i-1].d = i;
		p[i-1].w += e[(i-1) % 10];
	}
	sort(p, p + n, cmp);
	for(int i =0 ; i < k; i++){
		cout << p[i].i << " ";
	}
	return 0;
} 

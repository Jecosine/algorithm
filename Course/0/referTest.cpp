#include<iostream>
#include<cstdio>
void swap2(int *a, int *b){
    int t;
    t = *a;
    *a = *b;
    *b = t;
}
int fibonacci(int k, int m){
    if (m < k-1)
        return 0;

    if (m == k-1)
        return 1;
    int s = 0;
    for(int i = m - k; i < m; i++){
        s += fibonacci(k, i);
    }
    return s;
}

using namespace std;
int main(){
    printf("%d", fibonacci(2, 9));
    return 0;
}

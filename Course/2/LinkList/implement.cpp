#include <cstdio>
#include <cstdlib>

typedef enum {
    SUCCESS = 0,
    FAILED = 1,
    OVERFLOW = 2
} Status;

typedef struct LNode {
    char value;
    LNode *next;
} *Node;

namespace LINKLIST{
int Partition(Node* &L, int low, int high){
    char pivot = L[low] -> value;
    L[0] = L[low];
    while(low < high){
        while(low < high && L[high] -> value >= pivot)
            --high;
        L[low] = L[high];
        while(low < high && L[low] -> value <= pivot)
            ++low;
        L[high] = L[low];
    }
    L[low] = L[0];
    return low;
}
void Qsort(Node* &L, int low, int high){
    int pivot_p;
    if(low < high){
        pivot_p = Partition(L, low, high);
        Qsort(L, low, pivot_p - 1);
        Qsort(L, pivot_p + 1, high);
    }
}

Status Sort(Node &A){
    int length = A -> value, index = 1;
    Node pa = A -> next;
    Node a[length + 1];
    Node *p = a;
    while(pa){
        a[index++] = pa;
        pa = pa -> next;
    }
    Qsort(p, 1, length);
    pa = A -> next;
    index = 1;
    while(index < length){
        char prev = a[index] -> value;

        int temp = index;
        index++;
        while((index <= length) && (a[index] -> value == prev))
            index ++;
        if(index > length){
            index = temp;
            break;
        }

        a[temp] -> next = a[index];

    }
    A -> next = a[1];
    a[index] -> next = NULL;

}

void build_from_input(Node &A){
    Node pa = A;
    int length = 0;
    char c;
    if(!pa)
        pa = new LNode;
    pa -> next = NULL;
    printf("Input set: ");
    c = getchar();
    while(c != '\n'){
        if (c < 97 || c > 122){
            c = getchar();
            continue;
        }
        ++length;
        if (!(pa -> next)){
            pa -> next = new LNode;
            pa = pa -> next;
            pa -> next = NULL;
        }
        pa -> value = c;
        c = getchar();
    }
    A -> value = length;
}

void print_linklist(Node p){
    p = p -> next;
    while(p){
        printf("%c ", p -> value);
        if (p -> next)
            printf("-> ");
        p = p -> next;
    }
    printf("\n");
}

Status Difference(Node A, Node B, Node &C){
    int length = 0;
    Node pa = A -> next, pb = B -> next, pc = C;
    while (pa && pb){
        //interesting
        while(pb && (pb -> value < pa -> value))
            pb = pb -> next;
        if(pb == NULL)
            break;
        if (pa -> value != pb -> value){
            pc -> next = new LNode;
            pc = pc -> next;
            pc -> value = pa -> value;
            pc -> next = NULL;
            length++;
        }
        pa = pa -> next;
    }
    while(pa){
        pc -> next = new LNode;
        pc = pc -> next;
        pc -> value = pa -> value;
        pc -> next = NULL;
        pa = pa -> next;
        length++;
    }
    C -> value = length;
    return SUCCESS;
}
Status Intersection(Node A, Node B, Node &C){
    Node pa = A -> next, pb = B -> next, pc = C;
    int length = 0;
    while (pa && pb){
        //interesting
        while(pb && (pb -> value < pa -> value))
            pb = pb -> next;
        if(pb == NULL)
            break;
        if (pa -> value == pb -> value){
            pc -> next = new LNode;
            pc = pc -> next;
            pc -> value = pa -> value;
            pc -> next = NULL;
            length++;
        }
        pa = pa -> next;
    }
    C -> value = length;
    return SUCCESS;
}
Status Union(Node A, Node  B, Node &C){
    Node pa = A -> next, pb = B -> next, pc = C;
    char cur, last = -1;
    int length = 0;
    while (pa && pb){
        //interesting
        if(pa -> value < pb -> value){
            cur = pa -> value;
            pa = pa -> next;
        }
        else{
            cur = pb -> value;
            pb = pb -> next;
        }
        if(cur != last){
            pc -> next = new LNode;
            pc = pc -> next;
            pc -> value = cur;
            pc -> next = NULL;
            length++;
            last = cur;
        }
    }
    Node p;
    (p = pa) || (p = pb);
    while(p){
        cur = pc -> value;
        if(cur != last){
            pc -> next = new LNode;
            pc = pc -> next;
            pc -> value = p -> value;
            pc -> next = NULL;
            length++;
        }
        p = p -> next;
    }
    C -> value = length;
    return SUCCESS;
}

}
using namespace LINKLIST;
int main(){
    Node A = new LNode;
    Node B = new LNode;
    Node C = new LNode;
    Node D = new LNode;
    Node E = new LNode;
    A -> next = B -> next = C -> next = D -> next = E -> next = NULL;
    build_from_input(A);
    build_from_input(B);
    Sort(A);
    Sort(B);
    printf("Sorted A: ");
    print_linklist(A);
    printf("Sorted B: ");
    print_linklist(B);

    Union(A, B, C);
    Intersection(A, B, D);
    Difference(A, B, E);

    printf("Union of A and B: ");
    print_linklist(C);
    printf("Intersection of A and B: ");
    print_linklist(D);
    printf("Difference of A and B: ");
    print_linklist(E);
    delete A;
    delete B;
    delete C;
    delete D;
    delete E;
    return 0;
}


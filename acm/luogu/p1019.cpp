#include<bits/stdc++.h>
using namespace std;

using namespace std;
string a[21];
int cl[21][21];
int visit[21];
int n;
int ans=0;
int commonlenth(string a,string b)  
{
	int i,j,k;
	int minlen=0x3f3f3f3f;
	for(k=1;k<=a.length();k++){
		i=a.length()-1;
		j=k-1;
		int flag=0;
		while(i>=0&&j>=0){
			if(a[i]==b[j]){
				i--;
				j--;
			}
			else{
				flag=1;
				break;
			}
		}
		if(!flag)
			minlen=min(minlen,k);
	}
	return minlen==0x3f3f3f3f?0:minlen;
	
}
void dfs(int lenth,int pre){
	ans=max(ans,lenth);
	for(int i=0;i<n;i++){
		if(visit[i]<2){ 
			if(cl[pre][i]>=1&&cl[pre][i]<a[i].length()&&cl[pre][i]<a[pre].length()){
				visit[i]++;
				dfs(lenth+a[i].length()-cl[pre][i],i);
				visit[i]--;
			}
		}
	}
}
int main(){
	memset(visit,0,sizeof(visit));
	memset(cl,0,sizeof(cl));
	cin>>n;
	int i,j;
	for(i=0;i<n;i++)
		cin>>a[i];
	
	int x,y;
	for(x=0;x<n;x++)
		for(y=0;y<n;y++)
			cl[x][y]=commonlenth(a[x],a[y]); 
		
	
	char s;
	cin>>s;
	for(i=0;i<n;i++){
		string temp=a[i];
		if(temp[0]==s){
			visit[i]++;
			dfs(temp.length(),i);
			visit[i]--;
		}
	}
	cout<<ans<<endl;
	return 0; 
}

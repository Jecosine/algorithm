#include <iostream>
#include <cstdio>

using namespace std;
bool mos(int i){
	int a,b,c;
	a = i / 1000;
	if (a>=100){		
		b = i % 1000;
		c = b/100+b/10%10*10+b%10*100;
		return a==c;
	}
	else {
		b = i % 100;
		c = b/10+b%10*10;
		return a==c;
	}
}
int main() {
	int t,sum,n;
	cin >> n;
	for(int i=10000;i<1000000;i++){
		sum = 0;
		if (mos(i)){
			t = i;
			while(t){
				sum += t%10;
				t/=10;
			}
			if (sum == n)
				cout<<i<<endl;
		}
	}
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
int M[110][110];
int n, m, t, c = 0;
int si, sj, ei,ej, ti, tj;
struct point {
	int x, y;
	int m[10][10];
	point(int fx, int fy){
		x = fx; y = fy;
	}
};
queue<point> q;

void move(int &x, int &y, int d){
	switch(d){
		case 1: x++;break;
		case 2: y++;break;
		case 3: x--;break;
		case 4: y--;break;
	}
}
bool judge(int x, int y){
	//if(x >= n || y >= m || x < 0 || y < 0 || M[x][y])
//		return false;
//	return true;
	return x>=0&&x<n&&y>=0&&y<m&&M[x][y]==0;
}
void mark(int x, int y){
	 M[x][y] = 1;
} 
void bfs(){
	int x, y;
	q.push(point(si-1, sj-1));
	while(!q.empty()){
		point tp = q.front();
		q.pop();
		for(int k = 1; k <= 4;k++){		
			x = tp.x; y = tp.y;
			move(x, y, k);
			
			if(judge(x, y)){
				//mark(x, y);
				M[x][y] = 1;
				q.push(point(x, y));
				if(x == ei-1 && y == ej-1){
					c++;
					M[x][y] = 0;
					//continue;
				}				 
			}
			
		}
	}
}
int main(){
	cin >> n >> m >> t;
	memset(M, 0, sizeof(M));
	cin >> si >> sj >> ei >> ej;
	
	for(int i = 0 ; i < t; i++){
		cin >> ti >> tj;
		M[ti-1][tj-1] = 1;
	}
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++)
			cout << M[i][j];
		cout << endl;
	}
	M[si-1][sj-1] =  1;
	bfs();
	cout << c <<endl;
		for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++)
			cout << M[i][j];
		cout << endl;
	}
	return 0;
}

#include "include/base.h"

namespace LINKLIST {
// Init link list in length 0
Status InitL(Node &A){
    if(A)
        return FAILED;
    A = new LNode;
    A -> next = NULL;
    return SUCCESS;
}
// Get node by index
Status GetItem(Node A, int index, Node &node){
    if (index == 0){
        node = A;
        return SUCCESS;
    }
    if (GetItem(A, index - 1, node) == FAILED)
        return FAILED;
    node = node -> next;
    if(!node)
        return FAILED;
    return SUCCESS;
}
// Locate node with specified value
Status Locate(Node A, char value, Node &node){
    Node pa = A;
    while(pa){
        if(pa -> value == value){
            node = pa;
            return SUCCESS;
        }
    }
    return FAILED;
}

Status Insert(Node A, int index, char value){
    Node pa = A;
    int i = 0;
    while(pa && i < index - 1){
        pa = pa -> next;
        i++;
    }
    if(!pa || i + 1 < index)
        return FAILED;
    Node node = new LNode;
    node -> next = pa -> next;
    pa -> next = node;
    node -> value = value;

    return SUCCESS;
}
Status Delete(Node A, int index){
    Node pa = A;
    int i = 0;
    while(pa && i < index - 1){
        pa = pa -> next;
        i++;
    }
    if(!pa || i + 1 < index)
        return FAILED;
    Node temp = pa -> next;
    pa -> next = pa -> next -> next;
    delete temp;
    return SUCCESS;
}
}

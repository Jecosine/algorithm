#include <bits/stdc++.h>
using namespace std;
typedef struct {
	int id;
	int score;
} P;
bool cmp(P p1, P p2){
	if(p1.score == p2.score) return p1.id < p2.id;
	return p1.score > p2.score;
}
int main(){
	int n, m;
	cin >> n >> m;
	P ps[n];
	for(int i = 0; i < n;i++)
		cin >> ps[i].id >> ps[i].score;
	sort(ps, ps + n, cmp);
	// plan to interview
	int t = m * 1.5, score = ps[t-1].score;
	// deal with same score
	//cout << "score" << ps[t+1].score <<endl;
	while(ps[t].score == score) t++;
	
	cout << score << " " << t << endl;
	for(int i = 0; i < t; i++){
		cout << ps[i].id << " " << ps[i].score << endl;
	}
	return 0;
}

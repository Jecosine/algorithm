#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>
#include <cmath>
using namespace std;
char parse(char i, int p1){
	if(p1 == 3) return '*';
	if(p1 == 1 || i < 96) return i;
	if(p1 == 2) return i - 32;	
}
string fill(char i, char j, int p1, int p2, int p3){
	if(i+1 >= j) return "";
	if(abs(i - j) > 36) return "";
	string t;
	char c = 1;
	if(p3 == 2){
		for(char c = j - 1; c > i; c--)
			for(int k = 0; k < p2; k++)
				t += parse(c, p1);
	}
	else {
		for(i = i + 1; i < j; i++)
			for(int k = 0; k < p2; k++)
				t += parse(i, p1);
	}
	return t;
} 

int main(){
	string s;
	int p1, p2, p3;
	cin >> p1 >> p2 >> p3;
	//getchar();
	cin >> s;
	int l = s.size();
	for(int i = 1;i < l-1; i++){
		if(s[i] == '-')
			if(s[i-1] < s[i+1]){
				string t = fill(s[i-1], s[i+1], p1, p2, p3);
				s = s.erase(i, 1);
				if(t.size() != 0){
					
					s.insert(i, t);
				}
			}
				
	}
	cout << s;
	// aBBCCDDEEFFGGHHIIJJKKLLMMNNOOPPQQRRSSTTUUVVWWXXYYz
	// aBBCCDDEEFFGGHHIIJJKKLLMMNNOOPPQQRRSSTTUUVVWWXXYYz
	return 0;
} 

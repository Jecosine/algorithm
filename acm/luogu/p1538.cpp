#include <bits/stdc++.h>
using namespace std;
int d[10][7] {
	{1, 1, 1, 1, 1, 1, 0},
	{0, 0, 0, 0, 1, 1, 0},
	{1, 0, 1, 1, 0, 1, 1},
	{1, 0, 0, 1, 1, 1, 1},
	{0, 1, 0, 0, 1, 1, 1},
	{1, 1, 0, 1, 1, 0, 1},
	{1, 1, 1, 1, 1, 0, 1},
	{1, 0, 0, 0, 1, 1, 0},
	{1, 1, 1, 1, 1, 1, 1},
	{1, 1, 0, 1, 1, 1, 1}	
};
void read_digit(int a[][7], char c, int index){
	c -= 48;
	for(int i = 0; i < 7; i++){
		a[index][i] = d[c][i];
	}		
}
int main(){
	int k;
	cin >> k;
	string s;
	cin >> s;
	int l = s.size();
	int a[l][7];
	memset(a, 0, sizeof(a));
	for(int i = 0; i < l; i++){
		read_digit(a, s[i], i);
	}
	// first line 	
	for(int i = 0;i < l; i++){
		cout << ' ';
		for(int j = 0; j < k; j++){
			if (a[i][0])
				cout << '-';
			else
				cout << ' ';
		}
		cout << ' ';
		cout << ' ';
	}
	cout <<endl;
	// next
	for(int i = 0; i < k; i++){
		for(int j = 0;j < l; j++){
			if(a[j][1])
				cout << '|';
			else
				cout << ' ';
			for(int m = 0; m < k; m++)
				cout << " ";
			if(a[j][5])
				cout << '|';
			else
				cout << ' ';
			cout << " ";
		}
		cout << endl;
	}
	// middle line
	for(int i = 0;i < l; i++){
		cout << " ";
		for(int j = 0; j < k; j++){
			if (a[i][6])
				cout << '-';
			else
				cout << ' ';
		}
		cout << " ";
		cout << " ";
	}
	cout << endl;
	// next
	for(int i = 0; i < k; i++){
		for(int j = 0;j < l; j++){
			if(a[j][2])
				cout << '|';
			else
				cout <<' ';
			for(int m = 0; m < k; m++)
				cout << " ";
			if(a[j][4])
				cout << '|';
			else
				cout << ' ';
			cout << " ";
		}
		cout << endl;
	}
	// bottom line
		for(int i = 0;i < l; i++){
		cout << " ";
		for(int j = 0; j < k; j++){
			if (a[i][3])
				cout << '-';
			else
				cout << ' ';
		}
		cout << " ";
		cout << " ";
	}
	//cout << endl;
	return 0;
}

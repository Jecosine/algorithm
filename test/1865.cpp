#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cstring>
using namespace std;

long M[201] = {0};

long fibo(int n){
    if(M[n] != 0)
        return M[n];
    M[n] = fibo(n-2) + fibo(n-1);
    return M[n];
}
int main(){
    int n = 0;
    printf("%ld", fibo(100));
    M[0] = 1;M[1] = 1;M[2] = 2;M[3] = 3;M[4] = 5;
    string s;
    scanf("%d", &n);
    long long a[n] = {0};
    getchar();
    for (int i = 0; i < n;i++){
        getline(cin, s);
        a[i] = fibo(s.size());
    }
    for(int i = 0; i < n; i++){
        printf("%ld", a[i]);
        if (i != n - 1)
            printf("\n");
    }

    return 0;
}

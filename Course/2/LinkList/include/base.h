#ifndef BASE_H
#define BASE_H

#include <cstdio>
#include <cstdlib>

typedef enum {
    SUCCESS = 0,
    FAILED = 1,
    OVERFLOW = 2
} Status;

typedef struct LNode {
    char value;
    LNode *next;
} *Node;


//// Base implement of LinkList
//Status InitL(Node &A);
//
//Status Get(Node A, int index, Node &node);
//
//Status Locate(Node A, char value, Node &node);
//
//Status Insert(Node A, int index, char value);
//
//Status Delete(Node A, int index);
//
//
////read and print link list from console input
//void build_from_input(Node &A);
//
//void print_linklist(Node &A);
//
//// Practice
//// A - B
//Status Difference(LNode &A, LNode &B, LNode &C);
//// A �� B
//Status Intersection(LNode &A, LNode &B, LNode &C);
//// A �� B
//Status Union(LNode &A, LNode &B, LNode &C);
#endif // BASE_H

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cmath>
using namespace std;
int main(){
    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++)
        cin >> a[i];
    // bubble
    int t = 0;
    for(int i = 0;i < n;i++){
        for (int j = i; j < n-1; j++){
            if (a[j] > a[j+1]){
                t = a[j];
                a[j] = a[j+1];
                a[j+1] = t;
            }
        }
    }

    // out
    for(int i = 0; i < n;i++)
        cout << a[i] << " ";
    return 0;
}

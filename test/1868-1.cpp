#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;
int main(){
    vector<int> quene;
    int n, sum = 0, c = 0;
    while(scanf("%d", &n) != EOF){
        for(int x = 1; x*x < n; x++){
            if (2 * n % x == 0){
                int y =  2 * n / x;
                //printf("%d\n", y);
                if((x + y) % 2 == 1)
                    c++;
            }
        }
        printf("%d\n", c);
        c = 0;
    }
    return 0;
}

#include <iostream>
#include <cstdio>

using namespace std;

int main() {
	long i;
	string result="";
	int c;
	cin >> i;
	if (!i){
		cout << '0';
		return 0;
	}
	while(i){
		c = i % 16;
		result += (c >= 10)? 'A'+c-10:'0'+c;
		i /= 16;
	}
	int l = result.size();
	for(int i=0;i<l;i++)
		cout<<result[l-i-1];
	return 0;
}

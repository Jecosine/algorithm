#include <bits/stdc++.h>
using namespace std;
typedef struct {
	int i;
	int s, cs;
	string name;
	bool l, w;
	int c;
	int total;
} P ;
P p[100];
bool cmp(P p1, P p2){
	if (p1.total == p2.total)
		return p1.i < p2.i;
	else 
		return p1.total > p2.total;
}
int main(){
	int n;
	char b;
	cin >> n;
	for(int i = 0; i < n; i++){
		p[i].i = i+1;
		cin >> p[i].name >> p[i].s >> p[i].cs;
		while(cin >> b && b == ' ');
		if (b == 'Y')
			p[i].l = true;
		else p[i].l = false;
		while(cin >> b && b == ' ');
		if (b == 'Y')
			p[i].w = true;
		else p[i].w = false;
		cin >> p[i].c;
	}
	int sum = 0;
	for(int i = 0; i < n; i ++){
		//printf("%s %d %d %d %d %d\n",p[i].name.c_str(), p[i].s,p[i].cs,p[i].l, p[i].w, p[i].c);
		// calc scholar ship
		p[i].total = 0;
		if(p[i].c && p[i].s > 80)
			p[i].total += 8000;
		if (p[i].s > 85 && p[i].cs > 80)
			p[i].total += 4000;
		if(p[i].s > 90)
			p[i].total += 2000;
		if(p[i].s > 85 && p[i].w)
			p[i].total += 1000;
		if(p[i].cs > 80 && p[i].l)
			p[i].total += 850;
		sum += p[i].total;
	}
	sort(p, p+n, cmp);
	cout << p[0].name << endl << p[0].total << endl << sum << endl;
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
using namespace std;

int main() {
	int n;
	cin >> n;
	int t;
	int mx=-10001, mn=10001, sum = 0;
	for(int i=0;i<n;i++){
		cin >> t;
		if(t > mx){
			mx = t;
		}
		if (t < mn){
			mn = t;
		}
		sum += t;
	}
	cout << mx << endl << mn << endl<<sum << endl;
	return 0;
}

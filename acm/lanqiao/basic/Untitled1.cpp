#include <iostream>

int main()
{
    int row, line;
    std::cin >> row >> line;

    char **ary = NULL;
    ary = new char*[row];
    for (int i = 0; i < row; i++)
    {
        ary[i] = new char[line];
    }

    for (int i = 0; i < row; i++)
    {
        ary[i][0] = 'A' + i;
        for (int j = 1; j <= i, j < line; j++)
        {
            ary[i][j] = ary[i][j-1] - 1;
        }
        for (int k = i + 1; k < line; k++)
        {
            ary[i][k] = ary[i][k-1] + 1;
        }
    }

    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < line; j++)
        {
            std::cout << (char)ary[i][j];
        }
        std::cout << std::endl;
    }
    return 0;
}

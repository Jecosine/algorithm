#include <iostream>
#include <vector>
#include <cstdio>
using namespace std;

int main()
{
    string s, t;
    getline(cin, s);
    getline(cin, t);
    int l = s.size();
    int sum = 0, maxCost, maxChar = 0, currentChar = 0;
    //vector<char> q;
    scanf("%d", &maxCost);
    for(int i = 0; i < l; i++){
        sum += (s[i] > t[i]) ? s[i] - t[i]: t[i] - s[i];
        if(sum <= maxCost){
            currentChar++;
            if(maxChar < currentChar)
                maxChar = currentChar;
        }
        else {
            maxChar--;
            sum -= s[i - currentChar];
            currentChar--;
        }
    }
    if(sum == 0)
        maxChar = 0;
    cout << maxChar << endl;
    return 0;
}


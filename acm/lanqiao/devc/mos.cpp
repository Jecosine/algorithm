#include <iostream>
#include <cstdio>

using namespace std;
bool mos(int i){
	int a,b,c;
	a = i/100;
	b = i % 100;
	c = b/10+b%10*10;
	return a==c;
}
int main() {
	for(int i=1000;i<10000;i++){
		if (mos(i)){
			cout<<i<<endl;
		}
	}
	return 0;
}

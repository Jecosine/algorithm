#include <bits/stdc++.h>
using namespace std;
int tree[1001][1001];
int main(){
  memset(tree, 0, sizeof(tree));
  int n, ans;
  cin >> n;
  // read
  for(int i = 0; i < n; i++)
    for( int j = 0; j < i+1; j++)
      cin >> tree[i][j];
  // Method 1
  // for (int i = n-2; i >= 0;i--){
  //   for(int j = 0; j <= i; j++){
  //     tree[i][j] += max(tree[i+1][j], tree[i+1][j+1]);
  //   }
  // }
  // ans = tree[0][0];
  
  // Method 2
  for (int i = 1; i < n; i++){
    for(int j = 0; j <= i; j++){
      if(j == 0)
        tree[i][j] += tree[i-1][j];
      else if (j == i)
        tree[i][j] += tree[i-1][j-1];
      else
        tree[i][j] += max(tree[i-1][j], tree[i-1][j-1]);
    }
  }
  ans = tree[n-1][0];
  for(int i = 1; i < n; i++)
    if(tree[n-1][i] > ans)
      ans = tree[n-1][i];
  
  cout << ans;
  // debug: print triangle after calculation
  // for(int i = 0; i < n; i++){
  //   for( int j = 0; j < i+1; j++)
  //     cout << tree[i][j] << " ";
  //   cout << endl;
  // }  
  return 0;
}
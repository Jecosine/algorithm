#Insert-Sort
def insert_sort(increase = True):
	a = raw_input()
	a = [int(i) for i in a.split(' ') if i <> '']

	for i in range(1,len(a)):
		key = a[i]
		j = i-1
		while j >= 0 and a[j] > key:
			a[j+1] = a[j]
			j -= 1
		a[j+1] = key

	#format print (increasing mode)
	if increase:
		for i in range(len(a)):
			print a[i],
	else:
		for i in range(len(a)):
			print a[len(a)-i],

insert_sort()
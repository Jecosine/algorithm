#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
using namespace std;

int main() {
	int n, s;
	cin >> n;
	int a[n];
	memset(a, 0, sizeof(a));
	for(int i=0;i<n;i++)
		cin >> a[i];
	cin >> s;
	for(int i=0;i<n;i++)
		if(s == a[i]){
			cout << i+1;
			return 0;
		}
	cout << -1;
	return 0;
}

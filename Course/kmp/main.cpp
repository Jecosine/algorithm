#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;


void make_next(string a, int * p){
    string b;
    b.assign(a);
    int temp = 0 ,i = 0, j = -1;
    p[0] = -1;

    while(i < b.size()){
        if (j == -1 || a[i] == b[j]){
            i++;
            j++;
            p[i] = j;
        }
        else
            j = p[j];

    }

}
int main(){
    // read strings
    string a, b;
    printf("Input string a: ");
    getline(cin, a);
    printf("Input string b: ");
    getline(cin, b);
    int T[b.size()];
    int i = 0, j = 0, l = a.size(), *p;
    p = &T[0];
    make_next(b, p);
    printf("Next array: ");
    for (int i = 0; i < b.size();i++)
        printf("%d ",T[i]);
    while (i < l && j < (int)b.size()){
     if(j == -1 || a[i] == b[j]){
         i++;
         j++;
     }
     else
         j = T[j];
    }
    if (j == b.size())
        printf("\nFirst appear position at %d ",i - j);
    else
        printf("\nNot Found");
    return 0;
}

